/*
 * Мишуткин Александр, БПИ-192
 * Вариант-16
 * 28.11.2020
 *
 * Задача об инвентаризации по рядам. После нового года в
 * библиотеке университета обнаружилась пропажа каталога. После поиска и
 * наказания виноватых, ректор дал указание восстановить каталог силами
 * студентов. Фонд библиотека представляет собой прямоугольное помещение,
 * в котором находится M рядов по N шкафов по K книг в каждом шкафу.
 * Требуется создать многопоточное приложение, составляющее каталог. При
 * решении задачи использовать метод «портфель задач», причем в качестве
 * отдельной задачи задается составление каталога одним студентом для одного
 * ряда.
 */



#include <iostream>
#include <algorithm>
#include <thread>
#include <vector>
#include <memory>
#include <sstream>
#include <ctime>
#include <string>
#include <random>
#include <mutex>
#include <chrono>
#include <omp.h>

struct Timer {
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    std::chrono::time_point<std::chrono::high_resolution_clock> pause_start;


    void Start() {
        start = std::chrono::high_resolution_clock::now();
    }

    void Pause() {
        pause_start = std::chrono::high_resolution_clock::now();
    }

    void Continue() {
        start += (pause_start - std::chrono::high_resolution_clock::now());
    }

    [[nodiscard]] auto Finish() const {
        return (std::chrono::high_resolution_clock::now() - start);
    }

    [[nodiscard]] auto FinishInNanoSec() const {
        return std::chrono::duration_cast<std::chrono::nanoseconds>(this->Finish()).count();
    }
};

Timer timer; // NOLINT(cert-err58-cpp)

struct Task {
    size_t raw;
    size_t catalog_begin;

    Task(size_t raw, size_t catalog_begin) : raw(raw), catalog_begin(catalog_begin) {
    }
};

class Portfolio {
public:
    std::vector<Task> _tasks;

    explicit Portfolio(std::vector<Task> &&tasks) : _tasks(tasks) {
    }

    bool TryGetTask(const std::shared_ptr<Task> &task) {
        bool flag = true;
#pragma omp critical
        {
            if (_tasks.empty()) {
                flag = false;
            } else {
                *task = _tasks.back();
                _tasks.pop_back(); // critical
            }
        }
        return flag;
    }
};

std::string GenerateString(std::mt19937 &gen) {
    std::string string(gen() % 20 + 5, '\0');
    for (size_t i = 0; i < string.size(); i++) {
        if (gen() % 7 < 6 || i == 0 || string[i - 1] == ' ') {
            string[i] = gen() % 26 + 'a';
        } else {
            string[i] = ' ';
        }

        if (i == 0) {
            string[i] += 'A' - 'a';
        } else {
            if (string[i - 1] == ' ' && gen() % 10 < 9) {
                string[i] += 'A' - 'a';
            }
        }
    }

    return std::move(string);
}

std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> GenerateLibrary(size_t M, size_t N, size_t K) {
    std::mt19937 gen(std::time(nullptr) * 53);

    // Yes, it's not so pretty code, but it is done in best cpp traditions ))
    std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library(
            new std::shared_ptr<std::shared_ptr<std::string[]>[]>[M],
            std::default_delete<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]>());

    for (size_t raw = 0; raw < M; raw++) {

        library[raw] = std::shared_ptr<std::shared_ptr<std::string[]>[]>(new std::shared_ptr<std::string[]>[N],
                                                                         std::default_delete<std::shared_ptr<std::string[]>[]>());

        for (size_t book_case = 0; book_case < N; book_case++) {

            library[raw][book_case] = std::shared_ptr<std::string[]>(new std::string[K],
                                                                     std::default_delete<std::string[]>());

            for (size_t book = 0; book < K; book++) {
                library[raw][book_case][book] = GenerateString(gen);
            }
        }
    }

    if (N * M * K < 100) {
        std::cout << "Сгенерирована библиотека:" << std::endl;
        for (size_t raw = 0; raw < M; raw++) {
            std::cout << " - Ряд: " << raw + 1 << std::endl;
            for (size_t book_case = 0; book_case < N; book_case++) {
                std::cout << "   - Шкаф: " << book_case + 1 << std::endl;
                for (size_t book = 0; book < K; book++) {
                    std::cout << "     - " << library[raw][book_case][book] << std::endl;
                }
            }
        }
    } else {
        std::cout << "Библиотека сгенерирована, но слишком велика для вывода" << std::endl;
    }

    return std::move(library);
}

std::shared_ptr<std::string[]>
CommonSolution(const std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> &library, size_t M, size_t N,
               size_t K) {
    std::shared_ptr<std::string[]> catalog(new std::string[M * N * K], std::default_delete<std::string[]>());

    size_t cnt = 0;
    for (size_t raw = 0; raw < M; raw++) {
        timer.Pause();
        std::cout << " - Главный поток: обрабатывает ряд " << raw << std::endl;
        timer.Continue();
        for (size_t book_case = 0; book_case < N; book_case++) {
            for (size_t book = 0; book < K; book++) {
                std::stringstream stream;
                stream << "Книга \"" << library[raw][book_case][book]
                       << "\"" << std::string(26 - library[raw][book_case][book].length(), ' ') << "в ряду " << raw
                       << ", в шкафу " << book_case;

                catalog[cnt++] = stream.str();
            }
        }
    }

    timer.Pause();
    std::cout << " - Главный поток: запускает быструю сортировку" << std::endl;
    timer.Continue();

    std::sort(catalog.get(), catalog.get() + M * N * K);

    timer.Pause();
    std::cout << " - Главный поток: сортировка закончена" << std::endl;
    timer.Continue();

    return std::move(catalog);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "performance-unnecessary-value-param"

void ListRaw(std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library,
             std::shared_ptr<Portfolio> portfolio,
             std::shared_ptr<std::string[]> catalog,
             size_t N,
             size_t K) {
    std::shared_ptr<Task> task = std::make_shared<Task>(0, 0);
    while (portfolio->TryGetTask(task)) {
        size_t cnt = task->catalog_begin;
        size_t begin = cnt;
        size_t raw = task->raw;
        timer.Pause();
        int n = omp_get_thread_num();
#pragma omp critical
        {
            std::cout << " - Поток " << n << ": работает над рядом " << raw << std::endl;
        }
        timer.Continue();

        for (size_t book_case = 0; book_case < N; book_case++) {
            for (size_t book = 0; book < K; book++) {
                std::stringstream stream;
                stream << "Книга \"" << library[raw][book_case][book]
                       << "\"" << std::string(26 - library[raw][book_case][book].length(), ' ') << "в ряду " << raw
                       << ", в шкафу " << book_case;

                catalog[cnt++] = stream.str();
            }
        }

        std::sort(catalog.get() + begin, catalog.get() + cnt);
    }
}

#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma ide diagnostic ignored "performance-unnecessary-value-param"

std::shared_ptr<std::string[]>
MultiThreadSolution(std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library, size_t M,
                    size_t N,
                    size_t K) {
    std::vector<Task> task_list;
    std::shared_ptr<std::string[]> result(new std::string[N * M * K], std::default_delete<std::string[]>());

    size_t cnt = 0;
    for (size_t raw = 0; raw < M; raw++) {
        task_list.emplace_back(raw, cnt);
        cnt += N * K;
    }

    std::shared_ptr<Portfolio> portfolio = std::make_shared<Portfolio>(std::move(task_list));
    timer.Pause();
    std::cout << " - Портфель на " << M << " задач создан." << std::endl;
    timer.Continue();

#pragma omp parallel shared(N, K, portfolio, library, result) default(none)
    {
        ListRaw(library, portfolio, result, N, K);
    }

    size_t d = N * K;
    while (d < N * M * K) {
        size_t i;
#pragma omp parallel shared(N, K, result, std::cout, M, d, timer) private(i) default(none)
#pragma omp for
        for (i = 0; i < N * M * K; i += 2 * d) {
            timer.Pause();
            int n = omp_get_thread_num();
            int s_raw = std::round(static_cast<double>(std::min(i, N * M * K)) / static_cast<double>(N * K));
            int e_raw = std::round(static_cast<double>(std::min(i + 2 * d, N * M * K)) / static_cast<double>(N * K)) - 1;
#pragma omp critical
            {
                std::cout << " - Поток " << n << ": сливает ряды с " << s_raw << " по " << e_raw
                          << std::endl;
            }
            timer.Continue();
            std::shared_ptr<std::string[]> merge_result(new std::string[std::min(i + 2 * d, N * M * K) - i],
                                                        std::default_delete<std::string[]>());
            std::merge(result.get() + std::min(i, N * M * K),
                       result.get() + std::min(i + d, N * M * K),
                       result.get() + std::min(d + i, N * M * K),
                       result.get() + std::min(i + 2 * d, N * M * K),
                       merge_result.get());
            std::swap_ranges(merge_result.get(), merge_result.get() + (std::min(i + 2 * d, N * M * K) - i),
                      result.get() + std::min(i, N * M * K));
        }

        d *= 2;
    }

    return std::move(result);
}

#pragma clang diagnostic pop

std::string FormatTime() {
    std::string time = std::to_string(timer.FinishInNanoSec());
    if (time.length() >= 10) {
        return time;
    }
    std::string s(10 - time.length(), ' ');
    return s + time;
}

int main() {
    int M, N, K;
    M = N = K = -1;
    // M рядов по N шкафов по K книг в каждом шкафу
    while (M <= 0 || N <= 0 || K <= 0) {
        std::string s;
        std::cin.sync();
        std::getline(std::cin, s);
        std::stringstream stm(s);
        stm >> M >> N >> K;
        if (M <= 0 || N <= 0 || K <= 0) {
            std::cout << "Размеры библиотеки заданы некорректно, введите еще раз:" << std::endl;
        }
    }

    timer = Timer();

    std::shared_ptr<std::shared_ptr<std::shared_ptr<std::string[]>[]>[]> library = GenerateLibrary(M, N, K);

    std::cout << std::endl << "--------------------------------------------------------" << std::endl;
    std::cout << "Начинаем обычный алгоритм:" << std::endl;
    timer.Start();
    auto res = CommonSolution(library, M, N, K);
    std::cout << "Обычный алгоритм справился за          " << FormatTime() << " нс" << std::endl;

    std::cout << "--------------------------------------------------------" << std::endl;

    std::cout << "Начинаем ассинхронный алгоритм:" << std::endl;
    timer.Start();
    auto res_mt = MultiThreadSolution(library, M, N, K);
    std::cout << "Многопоточный алгоритм справился за    " << FormatTime() << " нс" << std::endl << std::endl;

    std::cout << "--------------------------------------------------------" << std::endl;

    if (M * N * K < 100) {
        std::cout << "Каталог обыного решения:" << std::endl;
        for (size_t i = 0; i < M * K * N; i++) std::cout << "  " << res[i] << std::endl;
        std::cout << "--------------------------------------------------------" << std::endl;
        std::cout << "Каталог многопоточного решения:" << std::endl;
        for (size_t i = 0; i < M * K * N; i++) std::cout << "  " << res_mt[i] << std::endl;
    } else {
        std::cout << "Каталог слишком большой для вывода";
    }
    return 0;
}
